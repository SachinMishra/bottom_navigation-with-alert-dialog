package com.ms.sam.bottom_navigation;

import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

   private TextView textView;
   AlertDialog.Builder ab;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView=findViewById(R.id.textView);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        ab=new AlertDialog.Builder(MainActivity.this);


    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                 switch (item.getItemId())
                 {
                case R.id.fragment1_ID:
                    textView.setText("welcome in Fragemnt1");
                    return true;
                case R.id.fragment2_ID:
                    textView.setText("Welcome in Fragment2");
                    return true;
                case R.id.fragment3_ID:
                    textView.setText("Hello in Fragament3");
                    return true;
            }
            return false;
        }
    };

    @Override
    public void onBackPressed()
    {

        ab.setTitle("Check");
        ab.setMessage("Do you want to exit..?");
        ab.setIcon(R.mipmap.ic_launcher_round);
        ab.setCancelable(false);
        ab.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                finish();
            }
        });
        ab.setNegativeButton("No", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.dismiss();

            }
        });


        ab.show();
//        super.onBackPressed();


    }

}
